package blk.testrestviewer;

import android.content.Context;
import android.os.Bundle;
import android.content.AsyncTaskLoader;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import blk.testrestviewer.Model.AviaCompany;
import blk.testrestviewer.Model.Flight;
import blk.testrestviewer.Model.Hotel;
import blk.testrestviewer.Model.ModelData;


class AsyncTaskLoaderRest extends AsyncTaskLoader <ModelData>{
    private static final String LOG_TAG = "my_tag";

    AsyncTaskLoaderRest(Context context, Bundle args) {
        super(context);
    }

    @Override
    public ModelData loadInBackground() {
        Log.d(LOG_TAG, "loadInBackground");
        return loadFromRest();
    }

    @Override
    public void forceLoad() {
        Log.d(LOG_TAG, "forceLoad");
        super.forceLoad();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.d(LOG_TAG, "onStartLoading");
        if (takeContentChanged())
            forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        Log.d(LOG_TAG, "onStopLoading");
    }


    private ModelData loadFromRest()
    {
        ArrayList<Flight> longSparseArrayFlight= new ArrayList<>();
        ArrayList<Hotel> longSparseArrayHotel= new ArrayList<>();
        ArrayList<AviaCompany> longSparseArrayAviaCompany = new ArrayList<>();
        RetrofitAdapter.ServerApi serverApi = RetrofitAdapter.getApi();
        try {
            longSparseArrayFlight.add(serverApi.getFlights().execute().body());
            Log.d(LOG_TAG, "onStartLoading FLIGHT");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        try {
            longSparseArrayHotel.add(serverApi.getHoteks().execute().body());
            Log.d(LOG_TAG, "onStartLoading HOTELS");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try {
            longSparseArrayAviaCompany.add(serverApi.getAviaCompanies().execute().body());
            Log.d(LOG_TAG, "onStartLoading COMPANIES");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        ModelData md = new ModelData();
        md.setFlightCollection(longSparseArrayFlight);
        md.setHotelCollection(longSparseArrayHotel);
        md.setAviaCompainCollection(longSparseArrayAviaCompany);
        return md;
    }
}
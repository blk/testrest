package blk.testrestviewer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;

import blk.testrestviewer.Model.ModelsForView.CompanyForView;
import blk.testrestviewer.R;

public class DialogFilterFragment extends DialogFragment {
    public interface onChangeFilter{
        void filterConfirmed(ArrayList <FilterObject> listCompanies);
    }
    private ArrayList<FilterObject> listBlocks;
    private ArrayList<CompanyForView> listCompanies; // пока явно этот лист , мб три листа сделаю

    public onChangeFilter onConfirmFilter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the EditNameDialogListener so we can send events to the host
            onConfirmFilter = (onChangeFilter) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement onDismissDialog");
        }
    }

    private void cleanBadFilters(){
        for (int i = 0 ;i < listBlocks.size();){
            if (listBlocks.get(i).isNeedToShow())
                listBlocks.remove(i);
            else
                i++;
        }
    }
    @Override
    public void onDestroy() {
        cleanBadFilters();
        onConfirmFilter.filterConfirmed(listBlocks);
        super.onDestroy();

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        if (getArguments() !=null){
            this.listCompanies = getArguments().getParcelableArrayList("ListCompanies");
            this.listBlocks = getArguments().getParcelableArrayList("ListBlocks");
        } else {
            this.dismiss();
            //this.listCompanies = new ArrayList<>();
            //this.listBlocks = new ArrayList<>();
        }
        AlertDialog.Builder b=  new  AlertDialog.Builder(getActivity())
                .setPositiveButton("Применить",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // do something...
                                dialog.dismiss();
                            }
                        }
                );


        View v = getActivity().getLayoutInflater().inflate(R.layout.dialogfragment,null);
        RecyclerView mrv = v.findViewById(R.id.recyclerDialog);
        mrv.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        RecycleAdapter myadapter = new RecycleAdapter(this.getActivity(),listCompanies);
        mrv.setAdapter(myadapter);
        b.setView(v);
        return b.create();
    }

    public  class RecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private int lastSelectedPosition = -1;
        long price;
        public RecycleAdapter(Context _mContext, ArrayList<CompanyForView> _mData) {
            //Log.i("LOGER","RecyclerViewAdapter(Context _mContext, ArrayList<Product>  _mData)");
            this.mContext = _mContext;
            this.mData = _mData;
        }
        private final ArrayList<CompanyForView> mData;
        private final Context mContext;

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.dialogitemfilterrecycler,parent,false);
            //LayoutInflater mInflater = LayoutInflater.from(mContext);
            return new MyViewHolderListDialog(v);

        }
        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            MyViewHolderListDialog hldr = (MyViewHolderListDialog) viewHolder;


            hldr.checkBox.setText(String.valueOf(mData.get(i).getCompanyName()));
            hldr.id = mData.get(i).getId();
            hldr.index = i;

            FilterObject fo = new FilterObject(i,CompanyForView.class);
            fo.needToShow = false;
            if (listBlocks.contains(fo)){
                hldr.checkBox.setChecked(false);
            }
           // hldr.radioButton.setText(mData.get(i).getCompanyForView().getCompanyName());
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public class MyViewHolderListDialog extends  RecyclerView.ViewHolder  {
            //TextView tvName;
            final CheckBox checkBox;
            long id;
            int index;
            final View view;
            public MyViewHolderListDialog (View itemView){
                super(itemView);
                view = itemView;
                Log.i("LOGER","MyViewHolder (View itemView)");
                //tvName = itemView.findViewById(R.id.nameFilterItem);
                checkBox = itemView.findViewById(R.id.checkboxFilter);
                checkBox.setChecked(true);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        CompanyForView cfv = mData.get(index);
                        FilterObject fo = new FilterObject(cfv.getId(),cfv.getClass());
                        fo.setNeedToShow(isChecked);
                        if (listBlocks.contains(fo)){
                            listBlocks.set(listBlocks.indexOf(fo),fo);
                        } else {
                            listBlocks.add(fo);
                        }

                    }
                });
            }

        }
    }

}
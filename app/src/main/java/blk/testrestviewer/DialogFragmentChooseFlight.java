package blk.testrestviewer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import blk.testrestviewer.Model.ModelsForView.FlightForView;
import blk.testrestviewer.R;

public class DialogFragmentChooseFlight extends DialogFragment {
    public interface onDismissDialog{
        void dialogDismiss(long id,int idx);
    }

    private final String LOG_TAG = "myLogs";
    private ArrayList<FlightForView> list;
    private long priceOfHotel;
    private long hotelId;
    private int choosingIndex = -1;
    private onDismissDialog onDismissDialogInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onDismissDialogInterface = (onDismissDialog) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement onDismissDialog");
        }
    }


    @Override
    public void onDestroy() {
        onDismissDialogInterface.dialogDismiss(hotelId,choosingIndex);
        super.onDestroy();

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        if (getArguments() !=null){
            this.list = getArguments().getParcelableArrayList("ListFlight");
            this.priceOfHotel = getArguments().getLong("PriceHotel");
            this.hotelId = getArguments().getLong("idHotel");
        } else {
            this.dismiss();
        }

        AlertDialog.Builder b=  new  AlertDialog.Builder(getActivity())
                .setPositiveButton("Применить",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // do something...
                                dialog.dismiss();
                            }
                        }
                );

        View v = getActivity().getLayoutInflater().inflate(R.layout.dialogfragment,null);
        RecyclerView mrv = v.findViewById(R.id.recyclerDialog);
        mrv.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        RecycleAdapter myadapter = new RecycleAdapter(this.getActivity(),list,priceOfHotel);
        mrv.setAdapter(myadapter);
        b.setView(v);
        return b.create();
    }

    class RecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private int lastSelectedPosition = -1;
        private final ArrayList<FlightForView> mData;
        private final long price;

        RecycleAdapter(Context _mContext, ArrayList<FlightForView> _mData, long price) {
            //Log.i("LOGER","RecyclerViewAdapter(Context _mContext, ArrayList<Product>  _mData)");
            this.mData = _mData;
            this.price = price;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.dialogitemforrecycler,parent,false);
            return new MyViewHolderListDialog(v);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            MyViewHolderListDialog hldr = (MyViewHolderListDialog) viewHolder;
            hldr.tvName.setText(String.valueOf(mData.get(i).getPrice()+price));
            hldr.radioButton.setText(mData.get(i).getCompanyForView().getCompanyName());
            hldr.radioButton.setChecked(lastSelectedPosition == i);
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        class MyViewHolderListDialog extends  RecyclerView.ViewHolder  {
            final TextView tvName;
            final RadioButton radioButton;
            final View view;
            MyViewHolderListDialog(View itemView){
                super(itemView);
                view = itemView;
                Log.i("LOGER","MyViewHolder (View itemView)");
                tvName = itemView.findViewById(R.id.nameFlight);
                radioButton = itemView.findViewById(R.id.radio);
                radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lastSelectedPosition = getAdapterPosition();
                        choosingIndex = lastSelectedPosition;
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }
}
package blk.testrestviewer;

import android.os.Parcel;
import android.os.Parcelable;

public class FilterObject implements Parcelable {
    private long idObj;
    boolean needToShow;
    private Class classObject;

    public boolean isNeedToShow() {
        return needToShow;
    }

    public void setNeedToShow(boolean needToShow) {
        this.needToShow = needToShow;
    }

    public FilterObject(long idObj, Class classObject) {
        this.idObj = idObj;
        this.classObject = classObject;
        this.needToShow = true;
    }

    public long getIdObj() {
        return idObj;
    }

    public void setIdObj(long idObj) {
        this.idObj = idObj;
    }

    public Class getClassObject() {
        return classObject;
    }

    public void setClassObject(Class classObject) {
        this.classObject = classObject;
    }

    private FilterObject(Parcel in) {
        idObj = in.readLong();
        needToShow = in.readByte() != 0x00;
        classObject = (Class) in.readValue(Class.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(idObj);
        dest.writeByte((byte) (needToShow ? 0x01 : 0x00));
        dest.writeValue(classObject);
    }
    @Override
    public boolean equals(Object object) {
        boolean sameSame = false;
        if (object != null && object instanceof FilterObject) {
            sameSame = ((this.idObj == ((FilterObject) object).idObj)&&
                    (this.classObject.equals(((FilterObject) object).classObject)));
        }
        return sameSame;
    }
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<FilterObject> CREATOR = new Parcelable.Creator<FilterObject>() {
        @Override
        public FilterObject createFromParcel(Parcel in) {
            return new FilterObject(in);
        }

        @Override
        public FilterObject[] newArray(int size) {
            return new FilterObject[size];
        }
    };
}
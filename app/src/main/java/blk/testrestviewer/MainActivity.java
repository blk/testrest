package blk.testrestviewer;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import blk.testrestviewer.Model.Flight;
import blk.testrestviewer.Model.ModelData;
import blk.testrestviewer.Model.ModelsForView.HotelForView;

public class MainActivity extends AppCompatActivity implements DialogFragmentChooseFlight.onDismissDialog ,DialogFilterFragment.onChangeFilter,LoaderManager.LoaderCallbacks<ModelData> {

    private final int LOADER_ID = 1;
    private ModelData md;
    private Context mContext;
    private ArrayList<FilterObject> filterObjectArrayList;
    private RecyclerViewAdapter myadapter;
    Loader<ModelData> mLoader;
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("MODEL",md);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_main);
        List<Flight> flights = new ArrayList<>();
        md = new ModelData();
        mLoader = getLoaderManager().initLoader(LOADER_ID, null, this);
        if (savedInstanceState!=null){
            md = savedInstanceState.getParcelable("MODEL");
            initViewersandHolders();
        } else {
            mLoader = getLoaderManager().initLoader(LOADER_ID,  null, this);
            mLoader.onContentChanged();
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoader.onContentChanged();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.actionFilter) {
            if (md != null) {
                Bundle args = new Bundle();
                args.putParcelableArrayList("ListBlocks", filterObjectArrayList);
                args.putParcelableArrayList("ListCompanies", md.getCompanyViewsAsList());

                DialogFilterFragment dialog = new DialogFilterFragment();
                dialog.setArguments(args);
                dialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "custom");
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.popupfiltermenu, menu);
        return true;
    }

    private void initViewersandHolders() {
        RecyclerView mrv = findViewById(R.id.recycler);
        filterObjectArrayList= new ArrayList<>();
        if (md!=null) {
            ArrayList<HotelForView> lst = md.getHotelViewAsList(filterObjectArrayList);
            myadapter = new RecyclerViewAdapter(this, lst, filterObjectArrayList);
            mrv.setLayoutManager(new GridLayoutManager(this, 1));
            mrv.setAdapter(myadapter);
        }
    }

    @Override
    public void dialogDismiss(long id, int idx) {
       // Тут получаем айди отеля и индекс перелета(радиобаттон)
        //Toast.makeText(this,"Hotel id in map = "+String.valueOf(id) + " IndexRadioBu = " + String.valueOf(idx),Toast.LENGTH_LONG).show();
    }

    @Override
    public void filterConfirmed(ArrayList<FilterObject> listCompanies) {
        ArrayList<FilterObject> listFilter = new ArrayList<>(listCompanies);
        ArrayList <HotelForView> hfv = md.getHotelViewAsList(listFilter);
        filterObjectArrayList = listFilter;
        myadapter.updatedata(hfv,listFilter);
    }

    @Override
    public Loader<ModelData> onCreateLoader(int id, Bundle args) {
        Loader mLoader = null;
        // условие можно убрать, если вы используете только один загрузчик
        if (id == LOADER_ID) {
            mLoader = new AsyncTaskLoaderRest(this, args);
        }
        return mLoader;
    }

    @Override
    public void onLoadFinished(android.content.Loader<ModelData> loader, ModelData data) {
        md = data;
        Log.d("my_tag", "MAIN ACTIVITY onFinishLoading");
        initViewersandHolders();
    }

    @Override
    public void onLoaderReset(android.content.Loader<ModelData> loader) {
        Log.d("my_tag", "MAIN ACTIVITY OnResetLoader");
    }

}

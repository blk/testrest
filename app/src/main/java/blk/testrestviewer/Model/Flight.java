package blk.testrestviewer.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flight {

    @SerializedName("flights")
    @Expose
    private List<Flight_> flights = null;

    public Flight() {
    }

    public Flight(List<Flight_> flights) {
        super();
        this.flights = flights;
    }

    public List<Flight_> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight_> flights) {
        this.flights = flights;
    }


    public class Flight_ {

        @SerializedName("id")
        @Expose
        private Long id;
        @SerializedName("companyId")
        @Expose
        private Long companyId;
        @SerializedName("departure")
        @Expose
        private String departure;
        @SerializedName("arrival")
        @Expose
        private String arrival;
        @SerializedName("price")
        @Expose
        private Long price;


        public Flight_() {
        }


        public Flight_(Long id, Long companyId, String departure, String arrival, Long price) {
            super();
            this.id = id;
            this.companyId = companyId;
            this.departure = departure;
            this.arrival = arrival;
            this.price = price;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getCompanyId() {
            return companyId;
        }

        public void setCompanyId(Long companyId) {
            this.companyId = companyId;
        }

        public String getDeparture() {
            return departure;
        }

        public void setDeparture(String departure) {
            this.departure = departure;
        }

        public String getArrival() {
            return arrival;
        }

        public void setArrival(String arrival) {
            this.arrival = arrival;
        }

        public Long getPrice() {
            return price;
        }

        public void setPrice(Long price) {
            this.price = price;
        }

    }
}

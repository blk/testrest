package blk.testrestviewer.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hotel {

    @SerializedName("hotels")
    @Expose
    private List<Hotel_> hotels = null;

    public Hotel() {
    }


    public Hotel(List<Hotel_> hotels) {
        super();
        this.hotels = hotels;
    }

    public List<Hotel_> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel_> hotels) {
        this.hotels = hotels;
    }

    public class Hotel_ {

        @SerializedName("id")
        @Expose
        private Long id;
        @SerializedName("flights")
        @Expose
        private List<Long> flights = null;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("price")
        @Expose
        private Long price;

        public Hotel_() {
        }

        public Hotel_(Long id, List<Long> flights, String name, Long price) {
            super();
            this.id = id;
            this.flights = flights;
            this.name = name;
            this.price = price;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public List<Long> getFlights() {
            return flights;
        }

        public void setFlights(List<Long> flights) {
            this.flights = flights;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getPrice() {
            return price;
        }

        public void setPrice(Long price) {
            this.price = price;
        }

    }
}
package blk.testrestviewer.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.util.LongSparseArray;

import java.util.ArrayList;

import blk.testrestviewer.FilterObject;
import blk.testrestviewer.Model.ModelsForView.CompanyForView;
import blk.testrestviewer.Model.ModelsForView.FlightForView;
import blk.testrestviewer.Model.ModelsForView.HotelForView;

public class ModelData implements Parcelable {
    public LongSparseArray<Flight.Flight_> getFlightCollection() {
        return flightCollection;
    }
    private final LongSparseArray<Flight.Flight_> flightCollection;
    private final LongSparseArray<Hotel.Hotel_> hotelCollection;
    private final LongSparseArray<AviaCompany.Company> aviaCompainCollection;
    private final ArrayList<FilterObject> filterObjectArrayList;
    public void setFlightCollection(@NonNull ArrayList<Flight> flightCollection) {
        if (flightCollection.size() > 0) {
            if (flightCollection.get(0) != null) {
                ArrayList<Flight.Flight_> inputList = (ArrayList<Flight.Flight_>) flightCollection.get(0).getFlights();
                for (Flight.Flight_ flight : inputList) {
                    this.flightCollection.put(flight.getId(), flight);
                }
            }
        }
    }
    public void setHotelCollection(@NonNull ArrayList<Hotel> hotelCollection) {
        if (hotelCollection.size() > 0) {
            if (hotelCollection.get(0)!=null) {
                ArrayList<Hotel.Hotel_> inputList = (ArrayList<Hotel.Hotel_>) hotelCollection.get(0).getHotels();
                for (Hotel.Hotel_ hotel : inputList)
                    this.hotelCollection.put(hotel.getId(),hotel);
            }
        }
    }
    public void setAviaCompainCollection(@NonNull ArrayList<AviaCompany> aviaCompainCollection) {
        if (aviaCompainCollection.size() > 0) {
            if (aviaCompainCollection.get(0) != null) {
                ArrayList<AviaCompany.Company> inputList = (ArrayList<AviaCompany.Company>) aviaCompainCollection.get(0).getCompanies();
                for (AviaCompany.Company company : inputList) {
                    this.aviaCompainCollection.put(company.getId(), company);
                }
            }
        }
    }

    public LongSparseArray<Hotel.Hotel_> getHotelCollection() {
        return hotelCollection;
    }


    public LongSparseArray<AviaCompany.Company> getAviaCompainCollection() {
        return aviaCompainCollection;
    }



    public ArrayList<CompanyForView> getCompanyViewsAsList(){
        ArrayList<CompanyForView> cfv = new ArrayList<>();

        for (int i = 0; i < aviaCompainCollection.size() ; i ++){
            cfv.add(new CompanyForView(aviaCompainCollection.valueAt(i).getId(),
                    aviaCompainCollection.valueAt(i).getName()));
        }
        return cfv;
    }
    public ArrayList<HotelForView> getHotelViewAsList(ArrayList<FilterObject> listCompanies){
        ArrayList<HotelForView> hfv = new ArrayList<>();

        for (int i = 0; i < hotelCollection.size() ; i ++){
            long id = hotelCollection.valueAt(i).getId();
            HotelForView hotelForView = getViewHotel(id,listCompanies);
            if (hotelForView!=null)
                hfv.add(hotelForView);
        }
        return hfv;
    }

    private HotelForView getViewHotel(long id,ArrayList<FilterObject> listCompanies){

        FilterObject foHotel = new FilterObject(id,HotelForView.class);
        if (listCompanies.contains(foHotel))
            return null;
        ArrayList <FlightForView >flightForView = new ArrayList<>();
        if (hotelCollection.get(id)!=null) {


                for (int i = 0; i < hotelCollection.get(id).getFlights().size(); i ++){
                    Flight.Flight_ f = flightCollection.get(hotelCollection.get(id).getFlights().get(i));
                    long compId = f.getCompanyId();
                    FilterObject foCompany = new FilterObject(compId, CompanyForView.class);
                    if (!listCompanies.contains(foCompany))
                        if (flightCollection.get(i)!=null) {
                            flightForView.add(new FlightForView(f.getId(),
                                new CompanyForView(aviaCompainCollection.get(compId).getId(),
                                        aviaCompainCollection.get(compId).getName()),
                                f.getDeparture(),
                                f.getArrival(),
                                f.getPrice()));
                    }
            }
            // Здесь скрываем отели без цен
            if (flightForView.size()==0)
                return null;

            return new HotelForView(
                    hotelCollection.get(id).getName(),
                    hotelCollection.get(id).getId(),
                    hotelCollection.get(id).getPrice(),
                    flightForView);
        } else return null;

    }


    public ModelData(){
        flightCollection = new LongSparseArray<>();
        hotelCollection = new LongSparseArray<>();
        aviaCompainCollection = new LongSparseArray<>();
        filterObjectArrayList = new ArrayList<>();
    }


    private ModelData(Parcel in) {
        flightCollection = (LongSparseArray) in.readValue(LongSparseArray.class.getClassLoader());
        hotelCollection = (LongSparseArray) in.readValue(LongSparseArray.class.getClassLoader());
        aviaCompainCollection = (LongSparseArray) in.readValue(LongSparseArray.class.getClassLoader());
        filterObjectArrayList = (ArrayList<FilterObject>) in.readValue(LongSparseArray.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(flightCollection);
        dest.writeValue(hotelCollection);
        dest.writeValue(aviaCompainCollection);
        dest.writeValue(filterObjectArrayList);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ModelData> CREATOR = new Parcelable.Creator<ModelData>() {
        @Override
        public ModelData createFromParcel(Parcel in) {
            return new ModelData(in);
        }

        @Override
        public ModelData[] newArray(int size) {
            return new ModelData[size];
        }
    };
}
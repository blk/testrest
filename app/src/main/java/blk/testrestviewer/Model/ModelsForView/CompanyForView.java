package blk.testrestviewer.Model.ModelsForView;

import android.os.Parcel;
import android.os.Parcelable;

public class  CompanyForView implements Parcelable {
    private final long id;
    private String companyName;
    public long getId() {
        return id;
    }
    public void setCompanyName(String name){
        companyName = name;
    }
    public String getCompanyName() {
        return companyName;
    }

    public CompanyForView(long id, String companyName) {
        this.id = id;
        this.companyName = companyName;
    }

    protected CompanyForView(Parcel in) {
        id = in.readLong();
        companyName = in.readString();
    }
    @Override
    public boolean equals(Object object) {
        boolean sameSame = false;
        if (object != null && object instanceof CompanyForView) {
            sameSame = this.id == ((CompanyForView) object).id;
        }
        return sameSame;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(companyName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CompanyForView> CREATOR = new Parcelable.Creator<CompanyForView>() {
        @Override
        public CompanyForView createFromParcel(Parcel in) {
            return new CompanyForView(in);
        }

        @Override
        public CompanyForView[] newArray(int size) {
            return new CompanyForView[size];
        }
    };
}
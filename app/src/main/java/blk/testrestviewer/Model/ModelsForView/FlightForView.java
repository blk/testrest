package blk.testrestviewer.Model.ModelsForView;

import android.os.Parcel;
import android.os.Parcelable;


public class FlightForView implements Parcelable {
    private final long id;
    private final CompanyForView companyForView;
    private final String departure;
    private final String arrival;
    private final long price;

    public long getId() {
        return id;
    }

    public CompanyForView getCompanyForView() {
        return companyForView;
    }

    public String getDeparture() {
        return departure;
    }

    public String getArrival() {
        return arrival;
    }

    public long getPrice() {
        return price;
    }

    public FlightForView(long id, CompanyForView companyForView, String departure, String arrival, long price) {
        this.id = id;
        this.companyForView = companyForView;
        this.departure = departure;
        this.arrival = arrival;
        this.price = price;
    }


    FlightForView(Parcel in) {
        id = in.readLong();
        companyForView = (CompanyForView) in.readValue(CompanyForView.class.getClassLoader());
        departure = in.readString();
        arrival = in.readString();
        price = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeValue(companyForView);
        dest.writeString(departure);
        dest.writeString(arrival);
        dest.writeLong(price);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<FlightForView> CREATOR = new Parcelable.Creator<FlightForView>() {
        @Override
        public FlightForView createFromParcel(Parcel in) {
            return new FlightForView(in);
        }

        @Override
        public FlightForView[] newArray(int size) {
            return new FlightForView[size];
        }
    };
}
package blk.testrestviewer.Model.ModelsForView;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class HotelForView implements Parcelable {
    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public ArrayList<FlightForView> getFlightForViews() {
        return flightForViews;
    }

    private final String name;

    public long getPrice() {
        return price;
    }

    private final long price;
    private final long id;
    private final ArrayList<FlightForView> flightForViews;

    public HotelForView(String name, long id, long price,ArrayList<FlightForView> flightForViews) {
        this.name = name;
        this.id = id;
        this.flightForViews = flightForViews;
        this.price = price;
    }

    protected HotelForView(Parcel in) {
        name = in.readString();
        price = in.readLong();
        id = in.readLong();
        if (in.readByte() == 0x01) {
            flightForViews = new ArrayList<>();
            in.readList(flightForViews, FlightForView.class.getClassLoader());
        } else {
            flightForViews = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeLong(price);
        dest.writeLong(id);
        if (flightForViews == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(flightForViews);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<HotelForView> CREATOR = new Parcelable.Creator<HotelForView>() {
        @Override
        public HotelForView createFromParcel(Parcel in) {
            return new HotelForView(in);
        }

        @Override
        public HotelForView[] newArray(int size) {
            return new HotelForView[size];
        }
    };
}
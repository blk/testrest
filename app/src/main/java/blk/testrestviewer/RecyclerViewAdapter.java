package blk.testrestviewer;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import blk.testrestviewer.Model.ModelsForView.CompanyForView;
import blk.testrestviewer.Model.ModelsForView.FlightForView;
import blk.testrestviewer.Model.ModelsForView.HotelForView;

class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    RecyclerViewAdapter(Context _mContext, ArrayList<HotelForView> _mData, ArrayList<FilterObject> filter) {
        //Log.i("LOGER","RecyclerViewAdapter(Context _mContext, ArrayList<Product>  _mData)");
        this.mContext = _mContext;
        this.mData = _mData;
        this.filterObjectArrayList = filter;
    }

    private final Context mContext;
    private final ArrayList<HotelForView> mData;
    private final ArrayList<FilterObject> filterObjectArrayList;





    public void updatedata(ArrayList <HotelForView> lst,ArrayList<FilterObject> filter){
        this.mData.clear();
        this.mData.addAll(lst);
        this.filterObjectArrayList.clear();
        this.filterObjectArrayList.addAll(filter);
        this.notifyDataSetChanged();

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        return new MyViewHolderList(mInflater.inflate(R.layout.itemviewlist,parent,false));

    }



    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        //holder.getItemViewType(); // можно проверять но необязательно.
        // то что написано ниже сейчас выглядит как перегруз кода и говнокод но.это.не.так.
                MyViewHolderList hldr = (MyViewHolderList) holder;
                hldr.tvHotelName.setText("Отель " + "\""+mData.get(position).getName()+ "\"");
                HotelForView hfv = mData.get(position);
                for (FilterObject fo:filterObjectArrayList){
                    if (hfv.getClass().equals(fo.getClassObject())){
                        if (hfv.getId() == fo.getIdObj()){
                            //то не показываем отели , пока не реализуем
                            break;
                        }
                    }else
                    if (fo.getClassObject().equals(CompanyForView.class)){
                        for (int i = 0 ; i < hfv.getFlightForViews().size();){
                            if (hfv.getFlightForViews().get(i).getCompanyForView().getId() == fo.getIdObj())
                                hfv.getFlightForViews().remove(i);
                            else
                                    i++;
                                //fvv.getCompanyForView().setCompanyName(fvv.getCompanyForView().getCompanyName()+"TODEL");
                            }
                        }
                }

                String countFlight = String.valueOf(hfv.getFlightForViews().size());
                String endOfWord = countFlight.equals("1")?"":
                        (countFlight.charAt(countFlight.length()-1) == '2' ||
                         countFlight.charAt(countFlight.length()-1) == '3' ||
                         countFlight.charAt(countFlight.length()-1) == '4')?"а":"ов";
                hldr.tvFlights.setText(countFlight + " вариант" + endOfWord + " перелета");
                long price = mData.get(position).getPrice();
                int countMin = 0;
                long minPrice = Long.MAX_VALUE;

                if (mData.get(position).getFlightForViews().size()!=0) {
                     minPrice = mData.get(position).getFlightForViews().get(0).getPrice();
                    for (FlightForView fvw : mData.get(position).getFlightForViews()) {
                        if (fvw.getPrice() < minPrice) {
                            countMin++;
                            minPrice = fvw.getPrice();
                        }
                    }
                }
                String beginofWord = countMin == mData.get(position).getFlightForViews().size()?"":"от ";
                if (minPrice == Long.MAX_VALUE){
                    hldr.tvPrice.setText("");
                } else {
                    hldr.tvPrice.setText(beginofWord + String.valueOf(
                            minPrice
                                    + price) + " р");

                }
                hldr.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Bundle args = new Bundle();
                        args.putParcelableArrayList("ListFlight",mData.get(position).getFlightForViews());
                        args.putLong("PriceHotel",mData.get(position).getPrice());
                        args.putLong("idHotel",mData.get(position).getId());

                        DialogFragmentChooseFlight dialog = new DialogFragmentChooseFlight();

                        dialog.setArguments(args);
                        dialog.show(((AppCompatActivity)mContext).getSupportFragmentManager(), "custom");


                    }
                });

            }

    @Override
    public int getItemCount() {
        return mData.size();
    }




    static class MyViewHolderList extends  RecyclerView.ViewHolder  {
        final TextView tvFlights;
        final TextView tvHotelName;
        final TextView tvPrice;
        final CardView cardView;
        final View view;
        MyViewHolderList(View itemView){
            super(itemView);
            view = itemView;
            Log.i("LOGER","MyViewHolder (View itemView)");
            tvHotelName = itemView.findViewById(R.id.hotelnameid);
            tvFlights = itemView.findViewById(R.id.flightsid);
            tvPrice =  itemView.findViewById(R.id.priceid);
            cardView =  itemView.findViewById(R.id.carditemid);
        }

    }
}

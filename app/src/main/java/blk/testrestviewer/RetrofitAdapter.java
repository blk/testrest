package blk.testrestviewer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import blk.testrestviewer.Model.AviaCompany;
import blk.testrestviewer.Model.Flight;
import blk.testrestviewer.Model.Hotel;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class RetrofitAdapter {
    private static final String baseUrl = "https://api.myjson.com";
    public interface ServerApi {
        @GET("/bins/zqxvw")
        Call<Flight> getFlights();

        @GET("/bins/12q3ws")
        Call<Hotel> getHoteks();

        @GET("/bins/8d024")
        Call<AviaCompany> getAviaCompanies();
    }

    public static ServerApi getApi(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(ServerApi.class);
    }

}
